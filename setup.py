import setuptools

setuptools.setup(
    name="structuregenerator",
    version='0.5',
    packages=setuptools.find_namespace_packages('.'),
    install_requires=[
        'matplotlib~=3.7.2',
        'numpy~=1.25.1',
        'scipy~=1.11.1',
        'tqdm~=4.65.0',
        'scikit-image~=0.21.0',
        'h5py~=3.9.0',
        'tsp-spanning~=0.2',
        'points_io @ git+https://bitbucket.org/4dnucleome/points_io.git@1.1.1'
    ],
    entry_points={
        'console_scripts': [
            'generator = structuregenerator.generator:main',
        ],
    },
)
