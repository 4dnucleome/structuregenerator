# README #

This is (not so) simple generator of different types of initial structures in PDB file format for chromatin and 
generic homopolymer simulations. It also contains tool for plotting distance map from generated structure.

### Dependencies and setup ###

python 3.x

	pip install git+https://bitbucket.org/4dnucleome/structuregenerator.git

### Usage ###

As commandline tool:

    $ generator
    usage: generator [-h] [-c X Y Z] [-C] [-o OUTPUT] [-p] [-s STEP]
                     type N [params [params ...]]
    
As python package:

    >>> from structuregenerator.generator import random_walk
    >>> from structuregenerator.points_io import save_points_as_pdb
    >>> 
    >>> points = random_walk(1000)
    >>> save_points_as_pdb(points, "my_structure.pdb")

List all possible types of structures:

    $ ./generator.py -h
    usage: generator.py [-h] [-c X Y Z] [-C] [-o OUTPUT] [-p] [-s STEP] [-d DISTORTION] [--two-dimensional] type N [params ...]
    
    Generate initial structure for polymer MD simulations
    
    positional arguments:
      type                  One of the listed below
      N                     number of beads
      params                Additional params type dependent params
    
    optional arguments:
      -h, --help            show this help message and exit
      -c X Y Z, --center X Y Z
                            Center in x, y, z. or in [0, 0, 0] if no value provided
      -C, --center_pdb      Center in in PDB box [4500, 4500, 4500].
      -o OUTPUT, --output OUTPUT
                            output PDB file name
      -p, --psf             generate PSF file
      -s STEP, --step STEP  step size (default: 1)
      -d DISTORTION, --distortion DISTORTION
                            structure distortion (in the units of step).
      --two-dimensional     2D structure
    
        Available types and its additional params:
        line:
            no extra args
        circle:
            z - z_stretch
        spiral_sphere:
            r - radius of sphere (float)
            c - controls number of turns (float)
        baseball:
            r - radius of sphere (float)
        gas:
            d - dimension of box (-d, d)
              or
            x1, y1, z1, x2, y2, z2 - box coordinates (floats)
        random_walk:
            no extra args
        self_avoiding_random_walk:
            two_dimensions True/False (default: False)
        constant_angle:
            theta - angle (degrees)
        img_gas:
            filename - npy file
            vx, vy, vz - voxel size
            threshold
        img_ds (points randomly drawn from image density):
            filename - cmap file
            threshold - int: percentile in image values 0-100
        img_tsp (points connected with traveling salesman algorithm):
            filename - cmap file
            threshold - int: percentile in image values 0-100


### Contact to main author ###

Michał Kadlof <m.kadlof@mini.pw.edu.pl>

### Significant contributors ###

Grzegorz Bokota <g.bokota@cent.uw.edu.pl>