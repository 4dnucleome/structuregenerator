#! /usr/bin/env python3
import matplotlib

matplotlib.use('TkAgg')

import argparse
import os

from matplotlib import pyplot as plt
from scipy.spatial.distance import pdist, squareform

from points_io import point_reader

__author__ = "Michał Kadlof <m.kadlof@cent.uw.edu.pl"


def draw(fname, dpi=100, cmap='viridis_r'):
    points = point_reader(fname)
    base, _ = os.path.splitext(fname)
    ofname = base + '.png'
    distances = squareform(pdist(points))
    plt.imshow(distances, cmap=cmap, origin='lower')
    plt.colorbar()
    plt.savefig(ofname, dpi=dpi, bbox_inches='tight', pad_inches=0.1)
    print('File {} saved.'.format(ofname))


def main():
    longer_help = """
    """

    parser = argparse.ArgumentParser(description="Draw distance map",
                                     epilog=longer_help, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("fname", help="PDB file name")
    parser.add_argument("-d", '--dpi', type=int, default=100, help="dpi")
    parser.add_argument('--cmap', default='viridis_r', help="colormap")
    args = parser.parse_args()
    draw(args.fname, dpi=args.dpi, cmap=args.cmap)


if __name__ == '__main__':
    main()
