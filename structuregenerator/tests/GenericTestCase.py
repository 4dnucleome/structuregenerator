import os
import shutil
import sys
import tempfile
import unittest


class GenericStructureGeneratorTestCase(unittest.TestCase):
    TESTS_DATA = os.path.join(os.getcwd(), 'tests/tests_data')

    @classmethod
    def _test_data_dir_exist(cls):
        return os.path.isdir(cls.TESTS_DATA)

    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        if not self._test_data_dir_exist():
            sys.exit("Directory with data used for tests is missing. Tests will not be run.")

    def tearDown(self):
        shutil.rmtree(self.test_dir)
