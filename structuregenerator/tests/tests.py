import os
import unittest

import numpy as np

import structuregenerator.generator as g
from structuregenerator.modeling_types.utils import random_versor
from structuregenerator.tests.GenericTestCase import GenericStructureGeneratorTestCase


# from generator_fast.generator_fast import interpolate_test


class StructuresReturnCorrectNumberOfPoints(GenericStructureGeneratorTestCase):
    # TODO
    pass


class StructuresAreBuildable(GenericStructureGeneratorTestCase):

    def test_spiral_sphere(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')
        gold_file = os.path.join(self.TESTS_DATA, 'test_spiral_sphere.pdb')

        class Args(object):
            N = 100
            center = False
            center_pdb = False
            output = tested_file
            params = [10.0, 1.0]
            type = 'spiral_sphere'
            step = 1.0
            distortion = 0.0
            psf = False

        g.build(Args)
        with open(tested_file) as f1, open(gold_file) as f2:
            t1 = f1.read()
            t2 = f2.read()
            self.assertMultiLineEqual(t1[:40] + t1[80:], t2[:40] + t2[80:])  # Timestamp cut of from comparison

    def test_baseball(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')
        gold_file = os.path.join(self.TESTS_DATA, 'test_baseball.pdb')

        class Args(object):
            N = 50
            center = False
            center_pdb = False
            output = tested_file
            params = [10.0]
            type = 'baseball'
            step = 1.0
            distortion = 0.0
            psf = False

        g.build(Args)
        with open(tested_file) as f1, open(gold_file) as f2:
            t1 = f1.read()
            t2 = f2.read()
        self.assertMultiLineEqual(t1[:40] + t1[80:], t2[:40] + t2[80:])  # Timestamp cut off from comparison

    def test_constant_angle(self):
        for i in range(-360, 360, 10):
            tested_file = os.path.join(self.test_dir, 'test.pdb')

            class Args(object):
                N = 50
                center = False
                center_pdb = False
                output = tested_file
                params = [i]
                type = 'constant_angle'
                step = 1.0
                distortion = 0.0
                psf = False

            g.build(Args)
            self.assertTrue(os.path.exists(tested_file))


class MyTestCase(GenericStructureGeneratorTestCase):

    def test_gas_does_not_contain_connect_records(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')

        class Args(object):
            N = 1000
            center = False
            center_pdb = False
            output = tested_file
            params = [50.0]
            type = 'gas'
            step = 1.0
            distortion = 0.0
            psf = False

        g.build(Args)
        with open(tested_file) as f:
            t = f.read()
        self.assertNotIn('CONECT', t)

    def test_psf_file_is_created_in_line_case(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')

        class Args(object):
            N = 2
            center = False
            center_pdb = False
            output = tested_file
            params = []
            type = 'line'
            step = 1.0
            distortion = 0.0
            psf = True

        g.build(Args)
        expected_file_name = os.path.join(self.test_dir, 'test.psf')
        assert os.path.isfile(expected_file_name)

    def test_psf_file_is_correct(self):
        pdb_file = os.path.join(self.test_dir, 'test.pdb')
        tested_file = os.path.join(self.test_dir, 'test.psf')
        gold_file = os.path.join(self.TESTS_DATA, 'test_psf_dimer.psf')

        class Args(object):
            N = 2
            center = False
            center_pdb = False
            output = pdb_file
            params = []
            type = 'line'
            step = 1.0
            distortion = 0.0
            psf = True

        g.build(Args)
        with open(tested_file) as f1, open(gold_file) as f2:
            t1 = f1.read()
            t2 = f2.read()
        self.assertMultiLineEqual(t1, t2)

    def test_centering(self):
        test_points = np.arange(12).reshape([4, 3])
        points_after_centering = g.center(test_points, np.array([0.0, 0.0, 0.0]))
        after_centering = np.mean(points_after_centering)
        self.assertEqual(after_centering, np.array([0, 0, 0]).all())

    def test_random_versor_is_length_one(self):
        x, y, z = random_versor()
        self.assertLess(abs(x ** 2 + y ** 2 + z ** 2 - 1), 1e-10)

    # def test_interpolation_in_cython(self):
    #     """Test if results from cython method is the same as from scipy interpn."""
    #     arr = np.zeros((2, 2, 2))
    #     arr[0, 1] = 1
    #     arr[1] = 2
    #     x, y, z = (0.7, 0.1, 0.7)
    #     cython_result = interpolate_test(arr, z, y, x)
    #     interpn_result = interpn(((0, 1), (0, 1), (0, 1)), arr, (z, y, x))
    #     self.assertTrue((cython_result - interpn_result < abs(1e-10)))


if __name__ == '__main__':
    unittest.main()
