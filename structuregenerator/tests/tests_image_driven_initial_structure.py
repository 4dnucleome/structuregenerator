import os
import sys
import tempfile
import unittest

import h5py
import numpy as np
import points_io

from structuregenerator import generator as g
from structuregenerator.tests.GenericTestCase import GenericStructureGeneratorTestCase


class ImageDrivenTests(GenericStructureGeneratorTestCase):
    test_dir = tempfile.mkdtemp()
    image_file = os.path.join('tests', 'tests_data', 'image.cmap')

    @classmethod
    def setUpClass(cls):
        # wykonajmy jedno modelowanie, na którym potem uruchomimy kilka testów.

        if not cls._test_data_dir_exist():
            sys.exit("Directory with data used for tests is missing. Tests will not be run.")
        cls.tested_file = os.path.join(cls.test_dir, 'test.pdb')

        class Args(object):
            N = 86
            center = False
            center_pdb = False
            output = cls.tested_file
            params = [cls.image_file, 15, 3]
            type = 'img_is'
            step = 1.0
            distortion = 0.0
            psf = False

        cls.args = Args
        g.build(Args)

    def test_img_is_build_something(self):
        self.assertTrue(os.path.exists(self.tested_file))

    def test_img_is_result_structure_size_fit_in_image(self):
        cmap_file = h5py.File(self.image_file, mode='r')
        a = np.array(cmap_file.get("Chimera/image1/data_zyx"))

        # Set reasonable defaults...
        voxel_dim = np.array([1, 1, 1])
        origin = np.array([0, 0, 0])

        # ...and override them if they are present in cmap file.
        try:
            voxel_dim = cmap_file.get("Chimera/image1").attrs["step"]
        except KeyError:
            pass
        try:
            voxel_dim = cmap_file.get("Chimera/image1").attrs["origin"]
        except KeyError:
            pass

        # Set limits in which all points should fit in.
        xlim = (origin[0] * voxel_dim[0], a.shape[2] * voxel_dim[0])
        ylim = (origin[1] * voxel_dim[1], a.shape[1] * voxel_dim[1])
        zlim = (origin[2] * voxel_dim[2], a.shape[0] * voxel_dim[2])

        self.assertTrue(os.path.exists(self.tested_file))
        points = points_io.point_reader(self.tested_file)
        for point in points:
            self.assertTrue(xlim[0] <= point[0] <= xlim[1])
            self.assertTrue(ylim[0] <= point[1] <= ylim[1])
            self.assertTrue(zlim[0] <= point[2] <= zlim[1])

    def test_img_is_distances_between_beads_are_reasonable(self):
        def _d(p1: np.ndarray, p2: np.ndarray) -> float:
            x1, y1, z1 = p1
            x2, y2, z2 = p2
            return ((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2) ** 0.5

        points = points_io.point_reader(self.tested_file)
        s = 0  # distance sum
        for i in range(len(points) - 1):
            a = points[i]
            b = points[i + 1]
            s += _d(a, b)
            self.assertTrue(abs(_d(a, b) - self.args.step) < 0.1)
        s /= (len(points) - 1)
        self.assertTrue(abs(s - 1) <= 0.01)


if __name__ == '__main__':
    unittest.main()
