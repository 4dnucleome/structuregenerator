#! /usr/bin/env python3

import argparse
import datetime
import sys
import time

import numpy as np
from points_io import save_points_as_pdb

__author__ = "Michał Kadlof <m.kadlof@cent.uw.edu.pl"

from structuregenerator.modeling_types import line, polymer_circle, spiral_sphere, baseball, random_gas, random_walk, self_avoiding_random_walk, constant_angle, img_tsp, center, distort, img_gas, img_ds
from structuregenerator.modeling_types.utils import spline


def build(args):
    # if args.N > 9999:
    #     sys.exit("9999 beads is the limit! You requested for {}".format(args.N))
    error_msg = "Wrong number of parameters for {{}}. Please consult {0} -h".format(sys.argv[0])
    render_connect = True  # default value for save_points_as_pdb()

    if args.type == "line":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = line(args.N)

    elif args.type == "circle":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        z = float(args.params[0])
        points = polymer_circle(args.N, z)

    elif args.type == "spiral_sphere":
        if len(args.params) != 2:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        r = float(args.params[0])
        c = float(args.params[1])
        points = spiral_sphere(args.N, r, c)

    elif args.type == "baseball":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        r = float(args.params[0])
        points = baseball(args.N, r)

    elif args.type == "gas":
        if len(args.params) not in [1, 6]:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = random_gas(args.N, args.params)
        render_connect = False

    elif args.type == "random_walk":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = random_walk(args.N)

    elif args.type == "self_avoiding_random_walk":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        points = self_avoiding_random_walk(args.N, step=args.step, bead_radius=args.step / 2,
                                           two_dimensions=args.two_dimensional)

    elif args.type == "constant_angle":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        fi = float(args.params[0])
        points = constant_angle(args.N, fi, step=args.step)

    elif args.type == "img_gas":
        if len(args.params) != 5:
            sys.exit(error_msg.format(args.type))
        file_name = args.params[0]
        voxel_size = np.array(args.params[1:4], dtype=np.float)
        threshold = float(args.params[4])
        points = img_gas(args.N, file_name, voxel_size, threshold)
        render_connect = False

    elif args.type == "img_ds":
        if len(args.params) != 2:
            sys.exit(error_msg.format(args.type))
        file_name = args.params[0]
        threshold = int(args.params[1])
        points = img_ds(args.N, file_name, threshold)

    elif args.type == "img_tsp":
        if len(args.params) != 2:
            sys.exit(error_msg.format(args.type))
        file_name = args.params[0]
        threshold = int(args.params[1])
        points = img_tsp(args.N, file_name, threshold)

    else:
        print("Wrong type {}. Please consult {} -h".format(args.type, sys.argv[0]))
        sys.exit(1)

    if args.center:
        points = center(points, args.center)
    elif not args.center and args.center_pdb:
        points = center(points, np.array([4500, 4500, 4500]))

    if args.interpolate:
        points = spline(points)

    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    remarks = [f'CREATED WITH STRUCTURE GENERATOR at {timestamp}']
    points = np.array(points)
    if args.distortion != 0:
        points = distort(points, args.step * args.distortion)
    save_points_as_pdb(points, args.output, save_psf=args.psf, render_connect=render_connect, remarks=remarks)


def main():
    longer_help = """
    Available types and its additional params:
    line:
        no extra args
    circle:
        z - z_stretch
    spiral_sphere:
        r - radius of sphere (float)
        c - controls number of turns (float)
    baseball:
        r - radius of sphere (float)
    gas:
        d - dimension of box (-d, d)
          or
        x1, y1, z1, x2, y2, z2 - box coordinates (floats)
    random_walk:
        no extra args
    self_avoiding_random_walk:
        two_dimensions True/False (default: False)
    constant_angle:
        theta - angle (degrees)
    img_gas:
        filename - npy file
        vx, vy, vz - voxel size
        threshold
    img_ds (points randomly drawn from image density):
        filename - cmap file
        threshold - int: percentile in image values 0-100
    img_tsp (points connected with traveling salesman algorithm):
        filename - cmap file
        threshold - int: percentile in image values 0-100
    """

    parser = argparse.ArgumentParser(description="Generate initial structure for polymer MD simulations",
                                     epilog=longer_help, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-c", '--center', nargs=3, metavar=('X', 'Y', 'Z'), type=float,
                        help="Center in x, y, z. or in [0, 0, 0] if no value provided")
    parser.add_argument("-C", '--center_pdb', action='store_true', help="Center in in PDB box [4500, 4500, 4500].")
    parser.add_argument("-o", '--output', default="initial_structure.pdb", help="output PDB file name")
    parser.add_argument("-p", '--psf', action="store_true", help="generate PSF file")
    parser.add_argument("-s", '--step', type=float, default=1.0, help="step size (default: 1)")
    parser.add_argument("-d", "--distortion", type=float, default=0.0, help="structure distortion (in the units of step).")
    parser.add_argument("-i", "--interpolate", action="store_true", help="perform spline interpolation")
    parser.add_argument("--two-dimensional", action="store_true", default=False, help="2D structure")
    parser.add_argument("type", help="One of the listed below")
    parser.add_argument("N", type=int, help="number of beads")
    parser.add_argument("params", metavar='params', nargs='*',
                        help="Additional params type dependent params")
    args = parser.parse_args()
    build(args)


if __name__ == '__main__':
    main()
