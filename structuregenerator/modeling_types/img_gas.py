import os
import sys

import numpy as np
from scipy.interpolate import interpn

'''Ta metoda nie jest obecnie supportowana. Jej zadanie przejęła metoda img_density.
Jednakże img_density nie ma pełnej zgodności z img_gas. (chociażby inny input). Dlatego ta metoda
pozostała jeszcze w repo, ponieważ może być użyteczna podczas przyszłej refaktoryzacji img_density.'''


def img_gas(n: int, filename: str, voxel_size: np.ndarray, threshold: float) -> np.ndarray:
    if not os.path.exists(filename):
        print(f'File {filename} does not exists.')
        sys.exit(1)
    img = np.load(filename)
    coords = [np.arange(img.shape[0]), np.arange(img.shape[1]), np.arange(img.shape[2])]
    points = []
    rand_max = np.array(img.shape) - 1
    while len(points) < n:
        new_points = np.random.uniform((0, 0, 0), rand_max, size=(20, 3))
        good_points = interpn(coords, img, new_points) > threshold
        points.extend(new_points[good_points][:, ::-1])
    points = points[:n]
    points = np.array(points)
    return points * voxel_size
