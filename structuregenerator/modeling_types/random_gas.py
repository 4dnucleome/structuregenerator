import numpy as np


def random_gas(n, params):
    params = [float(i) for i in params]
    if len(params) == 1:
        d = params[0]
        x = np.random.uniform(-d, d, n).reshape((n, 1))
        y = np.random.uniform(-d, d, n).reshape((n, 1))
        z = np.random.uniform(-d, d, n).reshape((n, 1))
    elif len(params) == 6:
        x1, y1, z1, x2, y2, z2 = params
        x = np.random.uniform(x1, x2, n).reshape((n, 1))
        y = np.random.uniform(y1, y2, n).reshape((n, 1))
        z = np.random.uniform(z1, z2, n).reshape((n, 1))
    else:
        raise ValueError('Incorrect input!')
    points = np.concatenate((x, y, z), axis=1)
    return points
