from typing import List

import numpy as np


def line(n: int) -> np.ndarray:
    points: List[List[float]] = []
    for i in range(n):
        points.append([i, 0., 0.])
    return np.array(points)
