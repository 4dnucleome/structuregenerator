import time
from itertools import combinations
from typing import Tuple, List

import numpy as np
import points_io
from points_io import save_points_as_marker_set
from scipy.ndimage import gaussian_filter
from scipy.spatial import Delaunay
from skimage.draw import line_nd
from tsp_spanning import tsp

from structuregenerator.modeling_types.img_ds import img_ds
from structuregenerator.modeling_types.utils import dist as d
from structuregenerator.modeling_types.utils import load_cmap


def weight_edges(edges_points: List[Tuple[np.ndarray, np.ndarray]],
                 img: np.ndarray,
                 voxel_size: np.ndarray,
                 alpha: float = 0.8) -> np.ndarray:
    distances = []
    signals = []

    for p1, p2 in edges_points:
        distance = d(p1, p2)
        distances.append(distance)

    for k, links in enumerate(edges_points):
        p1, p2 = links

        # Punkty we wsp. obrazu
        im1 = p1 / voxel_size
        im2 = p2 / voxel_size

        y_indices, x_indices, z_indices = line_nd(im1, im2, endpoint=True)
        mask = np.zeros(shape=img.shape)
        mask[y_indices, x_indices, z_indices] = 1
        mask = gaussian_filter(mask, sigma=0.8)
        signal = float(np.sum(img * mask))
        signal = signal / distances[k]
        signals.append(signal)

    distances = np.array(distances)
    signals = np.array(signals)
    signals = (signals - np.min(signals)) / (np.max(signals) - np.min(signals))
    weights = distances ** (1 - alpha) * (1 - signals) ** alpha
    return weights


def weights2distances(weights: np.ndarray, edges_points: List[Tuple[int, int]]) -> np.ndarray:
    """Dla listy wag i listy indeksów krawędzi zwraca kwadratową macierz dystansów.
    Dla krawędzi, których nie ma na liście macierz pozostanie wypełniona wartościami float('Inf')"""

    def _find_max_index(edges):
        max_index = edges[0][0]  # Początkowo ustawiamy pierwszą wartość jako maksimum
        for e1, e2 in edges:
            max_index = max(max_index, e1, e2)
        return max_index

    n = _find_max_index(edges_points) + 1
    distances = np.full((n, n), float('inf'))
    for i in range(len(weights)):
        e1, e2 = edges_points[i]
        distances[e1, e2] = weights[i]
    return distances


def convert_triangulation_to_edges(tri) -> Tuple[List[Tuple[int, int]], List[Tuple[np.ndarray, np.ndarray]]]:
    """Ta funkcja przetwarza wyniki triangulacji i zwraca listy krawędzi:
        edges_indices: krawędzie jako pary indeksów punktów (int, int)
        edges_points: krawędzie jako pary punktów (np.ndarray, np.ndarray)

        Na wejściu dostaje wynik funkcji scipy.spatial.Delaunay()
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.Delaunay.html
        """
    edges_indices: List[Tuple[int, int]] = []
    for simplex in tri.simplices:
        for pair in combinations(simplex, 2):
            a, b = pair
            # zamień kolejnością, żeby krawędzie były zawsze posortowane po indeksach
            if a > b:
                a, b = b, a
            edges_indices.append((a, b))

    # Usuń duplikaty i posortuj
    edges_indices = list(set(edges_indices))
    edges_indices = sorted(edges_indices, key=lambda x: (x[0], x[1]))

    # Utwórz listę krawędzi z informacją o współrzędnych zamiast o indeksach
    edges_points = []
    tmp_points = np.copy(tri.points)
    for a, b in edges_indices:
        p1 = tmp_points[a]
        p2 = tmp_points[b]
        edges_points.append((p1, p2))
    return edges_indices, edges_points


def img_tsp(n: int, filename: str, desired_percentile: int):
    """Struktura początkowa z użyciem punktów losowanych z gęstości obrazu i łączonych
    algorytmem TSP."""

    # Losowanie punktów z gęstości i wczytanie obrazka
    points = img_ds(n, filename, desired_percentile=desired_percentile)
    img, voxel_size = load_cmap(filename)

    # Triangulacja i przetworzenie wyników do postaci grafu
    triangulation = Delaunay(points)
    edges_indices, edges_points = convert_triangulation_to_edges(triangulation)

    # Obliczenie wag i przekształcenie ich w macierz dystansów
    weights = weight_edges(edges_points, img, voxel_size)
    distances = weights2distances(weights, edges_indices)

    normed_weights = 0.1 + ((weights - np.min(weights)) * (1 - 0.1)) / (np.max(weights) + np.max(weights))
    save_points_as_marker_set(points, 'triangulation.cmm', marker_set_name="Triangulation", marker_radii=0.2, links=edges_indices, links_colors=1 - weights, links_radii=(1 - normed_weights) * 0.1)

    print('TSP solver...')
    start_time = time.time()
    path: np.ndarray = tsp(distances)
    duration = time.time() - start_time
    print(f'Executed in {duration:0.3f} seconds')

    new_points = [0] * path.size
    for i in range(path.size):
        new_points[i] = points[path[i]]
    return new_points
