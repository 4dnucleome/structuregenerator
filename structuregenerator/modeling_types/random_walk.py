from typing import List

import numpy as np


def random_walk(n: int) -> np.ndarray:
    versors = np.random.uniform(-1, 1, 3 * (n - 1)).reshape(((n - 1), 3))
    points: List[List[float]] = [[.0, .0, .0]]
    for i in versors:
        points.append([points[-1][0] + i[0], points[-1][1] + i[1], points[-1][2] + i[2]])
    return np.array(points)
