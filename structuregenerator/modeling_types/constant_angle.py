import numpy as np
from scipy.linalg import orth


def constant_angle(n: int, theta: float, step: float = 1) -> np.ndarray:
    # noinspection PyShadowingNames,PyPep8Naming
    def _orthogonal_planes(ab: np.ndarray, B: float):
        # noinspection PyTypeChecker
        ab1: np.ndarray = ~(np.absolute(ab) < 10e-5)
        if ab1[0]:
            x = np.array([ab[1], -ab[0], 0])
            y = np.array([ab[2], 0, -ab[0]])
            ort = orth(np.array([x, y]).T)
            x, y = ort.T * B
            return x, y
        elif ab1[1]:
            x = np.array([1, 0, 0]) * B
            y = np.array([0, ab[2], -ab[1]])
            y = y / np.linalg.norm(y) * B
            return x, y
        else:
            return np.array([1, 0, 0]) * B, np.array([0, 1, 0]) * B

    theta = np.deg2rad(theta)
    points = [np.array([0, 0, 0]), np.array([step, 0, 0])]
    r = np.linalg.norm(points[1] - points[0])
    A = r * np.cos(np.pi - theta)
    B = r * np.sin(np.pi - theta)
    for i in range(n - 2):
        a = points[-2]
        b = points[-1]
        ab = b - a
        X, Y = _orthogonal_planes(ab, B)
        angle = 2 * np.pi * np.random.random()
        new_point = X * np.sin(angle) + Y * np.cos(angle) + (b + ab / np.linalg.norm(ab) * A)
        points.append(new_point)
    return np.array(points)

