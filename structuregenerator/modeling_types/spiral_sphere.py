import numpy as np
from numpy import sqrt, cos, sin


def spiral_sphere(n: int, r: float, c: float) -> np.ndarray:
    """based on: http://elib.mi.sanu.ac.rs/files/journals/vm/57/vmn57p2-10.pdf"""

    def calc_x(t): return sqrt(r ** 2 - t ** 2) * cos(t / c)

    def calc_y(t): return sqrt(r ** 2 - t ** 2) * sin(t / c)

    def calc_z(t): return t

    t_lin = np.linspace(-r, r, n)
    x = calc_x(t_lin).reshape((n, 1))
    y = calc_y(t_lin).reshape((n, 1))
    z = calc_z(t_lin).reshape((n, 1))
    points = np.concatenate((x, y, z), 1)
    return points
