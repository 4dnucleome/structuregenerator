import numpy as np

from structuregenerator.modeling_types.utils import load_cmap


def img_ds(n: int, input_path: str, desired_percentile: int = 0) -> np.ndarray:
    """Losowanie punktów z gęstości. Można wskazać percentyl, aby odciąć tło."""
    image_array, voxel_dim = load_cmap(input_path)
    percentile_value = np.percentile(image_array, desired_percentile)
    image_array[image_array < percentile_value] = 0
    total_density = np.sum(image_array)
    probabilities = image_array / total_density
    flat_probabilities = probabilities.ravel()
    random_indices = np.random.choice(np.arange(flat_probabilities.size), size=n, replace=False, p=flat_probabilities)
    indices_3d = np.unravel_index(random_indices, image_array.shape)
    indices_3d = np.array(indices_3d)
    points = indices_3d.T * voxel_dim
    return points
