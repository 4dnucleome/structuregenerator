import numpy as np
from numpy import pi, sin, cos


def baseball(n, r):
    # noinspection HttpUrlsUsage
    """based on: http://paulbourke.net/geometry/baseball/"""
    b = pi / 2
    a = 0.4  # shape parameter. Better not to change

    def calc_x(t): return r * sin(b - (b - a) * cos(t)) * cos(t / 2. + a * sin(2 * t))

    def calc_y(t): return r * sin(b - (b - a) * cos(t)) * sin(t / 2. + a * sin(2 * t))

    def calc_z(t): return r * cos(b - (b - a) * cos(t))

    t_lin = np.linspace(0, 4 * pi - (4 * pi) / n, n)
    x = calc_x(t_lin).reshape((n, 1))
    y = calc_y(t_lin).reshape((n, 1))
    z = calc_z(t_lin).reshape((n, 1))
    points = np.concatenate((x, y, z), 1)
    return points
