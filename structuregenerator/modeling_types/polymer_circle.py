import numpy as np
from numpy import pi, sin, cos


def polymer_circle(n: int, z_stretch: float = 0.0) -> np.ndarray:
    points = []
    angle_increment = 360 / float(n)
    radius = 1 / (2 * sin(np.radians(angle_increment) / 2.))  # assure distance 1
    z_stretch = z_stretch / n
    z = 0
    for i in range(n):
        x = radius * cos(angle_increment * i * pi / 180)
        y = radius * sin(angle_increment * i * pi / 180)
        if z_stretch != 0:
            z += z_stretch
        points.append((x, y, z))
    points = np.array(points)
    return points
