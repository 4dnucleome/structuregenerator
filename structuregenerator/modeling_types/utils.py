from typing import Tuple

import h5py
import numpy as np
from scipy import interpolate


def center(points: np.ndarray, centering_point: np.ndarray) -> np.ndarray:
    """Przesuwa środek ciężkości zestawu punktów do wskazanego punktu."""
    centering_point = np.array(centering_point)
    center_before_centering = np.mean(points, axis=0)
    centering_vector = centering_point - center_before_centering
    points = points + centering_vector
    return points


def dist(p1: np.ndarray, p2: np.ndarray) -> float:
    """Mierzy dystans w przestrzeni R^3"""
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2) ** 0.5  # faster than np.linalg.norm


def distort(points: np.array, scale: float) -> np.array:
    """Wprowadza losowe zaburzenie do podanego zestawu punktów."""
    n = points.shape[0]
    rv = random_versors(n) * scale
    points = points + rv
    return points


def load_cmap(path: str) -> Tuple[np.ndarray, np.ndarray]:
    """Wczytuje obraz mikroskopowy zapisany w formacie CMAP (UCSF Chimera map).

    Args:
        path (str): Path to cmap file.

    Returns:
        object: Tuple[image, voxel_size]
    """
    if path.endswith(".cmap"):
        image_array, max_bright, voxel_dim = read_cmap_image(path)
    else:
        input_format = "." + path.split(".")[-1]
        raise (Exception("Wrong data format: " + input_format + "/nSupported: .npy, .cmap, .tiff"))
    return image_array, voxel_dim


def point_between(point_1: np.ndarray, point_2: np.ndarray, prop: float = 0.5):
    """Calculates coordinates of in the middle of two points in R^3.

    Args:
        point_1 (three floats tuple) : point 1 coordinates
        point_2 (three floats tuple) : point 2 coordinates
        prop (float) : Proportion, where to split segment

    Returns:
        (three floats tuple) : point in the middle
    """
    x_1, y_1, z_1 = point_1
    x_2, y_2, z_2 = point_2
    return x_1 * (1 - prop) + x_2 * prop, y_1 * (1 - prop) + y_2 * prop, z_1 * (1 - prop) + z_2 * prop


def random_versor() -> np.ndarray:
    """Losuje wersor"""
    x = np.random.uniform(-1, 1)
    y = np.random.uniform(-1, 1)
    z = np.random.uniform(-1, 1)
    d = (x ** 2 + y ** 2 + z ** 2) ** 0.5
    return np.array([x / d, y / d, z / d])


def random_versors(n: int) -> np.array:
    """Losuje zestaw n wersorów"""
    rv = np.random.uniform(-1, 1, size=n * 3).reshape((n, 3))
    norm = (rv[:, 0] ** 2 + rv[:, 1] ** 2 + rv[:, 2] ** 2) ** 0.5
    norm = norm.reshape((n, 1))
    return rv / norm


def read_cmap_image(image):
    """Load CMAP image and define the spacing and maximal brightness of image"""
    cmap_file = h5py.File(image, mode='r')
    a = np.array(cmap_file.get("Chimera/image1/data_zyx"))
    try:
        voxel_dim = cmap_file.get("Chimera/image1").attrs["step"]
    except KeyError:
        voxel_dim = np.array([1, 1, 1])
    img = np.swapaxes(a, 0, 2)
    max_bright = img.max(initial=float('-inf'))
    return img, max_bright, voxel_dim


def spline(points: np.ndarray, ref_dist: float = 1.0) -> np.ndarray:
    """Performs spline interpolation out of list of points

    Args:
        points (list of three floats tuples) : points that will be used as spline nodes
        ref_dist (float) : desired distances between beads

    Returns:
        points (list of three floats tuples) : points after interpolation
    """
    points = np.asarray(points)
    points = points.transpose()
    tck, u, *_ = interpolate.splprep(points, s=0)
    new = interpolate.splev(np.linspace(0, 1, 5000000), tck)  # ta duża liczba jest ważna. Im większa, tym lepsza precyzja. i tym dłużej sie liczy.
    bla = list(zip(new[0], new[1], new[2]))
    p = [bla[0]]
    n = len(bla)
    previous_point = bla[0]
    distance_to_last_bead = 0
    for i in range(n):
        current_point = bla[i]
        loc_dist = dist(previous_point, current_point)
        distance_to_last_bead += loc_dist
        if distance_to_last_bead > ref_dist:
            proportion = (distance_to_last_bead - ref_dist) / loc_dist
            p.append(point_between(previous_point, current_point, prop=proportion))
            distance_to_last_bead = distance_to_last_bead - ref_dist
        previous_point = current_point
    return np.array(p)
